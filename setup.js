let types = [
    {
        name: 'relay',
    }
]

let items = [
    {
        id: 1,
        pin: 1,
        bus: 0x20,
        name: 'Steckdose 1',
        room: 'Küche',
        type: 'relay',
        state: {
            on: true,
        },
        events: [
            {
                type: 'timed',
                times: [
                    [0, 60 * 60 * 24 * 1000] // only ON times
                ]
            },
            {
                type: 'trigger',
                by: 5
            }
        ]
    },
    {
        id: 2,
        pin: 2,
        bus: 0x20,
        type: 'led',
        state: {
            on: true,
            colors: [{},{},{},{}] // rgb*50
        }
    }
]

