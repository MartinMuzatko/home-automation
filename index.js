const five = require('johnny-five')
const Raspi = require('raspi-io')
const board = new five.Board({
    io: new Raspi(),
})
const cors = require('cors')

const pins = require('./pins')

const express = require('express')

function createRelay(gpio) {
    return new five.Relay({
        pin: `GPIO${gpio}`,
        type: 'NC'
    }).off()
}

function createMotionDetector(gpio) {
    return new five.Motion({
        pin: `GPIO${gpio}`
    })
}

function assignOperation({type, gpio}) {
    let operations = {
        relay: createRelay,
        motion: createMotionDetector,
    }
    if (!operations[type]) return false
    return operations[type](gpio)
}

board.on('ready', function () {
    pins.forEach(pin => {
        pin.operation = assignOperation(pin)
    })

    let motion = getPinByGPIO(4).operation
    let relay1 = getPinByGPIO(21).operation

    motion.on('calibrated', function() {
        console.log('calibrated')
    })

    motion.on('motionstart', function() {
        relay1.on()
        console.log('motionstart')
    })
    
    motion.on('motionend', function() {
        relay1.off()
        console.log('motionend')
    })

    // alternaitve
    this.repl.inject({
        pins
    })
})

const app = express()

app.use(cors({origin: '*'}))

app.get('/pins', (req, res) => {
    res.send(pins.map(({operation, ...pin}) => ({
        ...pin,
        isOn: operation.isOn,
    })))
})

function getPinByGPIO(gpio) {
    gpio = parseInt(gpio)
    return pins.find(pin => pin.gpio == gpio)
}

app.get('/:id', (req, res) => {
    let isOn = getPinByGPIO(req.params.id).operation.isOn
    res.send(isOn)
})

app.get('/:id/on', (req, res) => {
    getPinByGPIO(req.params.id).operation.on()
    res.send('on')
})

app.get('/:id/off', (req, res) => {
    getPinByGPIO(req.params.id).operation.off()
    res.send('off')
})

app.listen(80)
