# Fix from fresh node modules

## Set I2C to 0

raspi-i2c - node module: hardcoded to use i2c 1 or 2 (dist/index.js, getDevice)
https://github.com/nebrius/raspi-i2c/blob/master/src/index.ts#L451

## Do not turn on all inverted relays on start

`
https://github.com/rwaldron/johnny-five/blob/master/lib/expander.js#L73