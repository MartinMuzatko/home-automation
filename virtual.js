const five = require('johnny-five')
const Raspi = require('raspi-io')
const board = new five.Board({
    io: new Raspi(),
})
const cors = require('cors')

const pins = require('./pins')

const express = require('express')

function createRelay(pin, board) {
    return new five.Relay({
        pin,
        board,
        type: 'NC'
    }).off()
}

function createMotionDetector(gpio) {
    return new five.Motion({
        pin: `GPIO${gpio}`
    })
}

function assignOperation({type, gpio}) {
    let operations = {
        relay: createRelay,
        motion: createMotionDetector,
    }
    if (!operations[type]) return false
    return operations[type](gpio)
}

board.on('ready', function () {
    // pins.forEach(pin => {
    //     pin.operation = assignOperation(pin)
    // })

    let virtual = new five.Board.Virtual(
        new five.Expander({
            controller:'MCP23017',
            address: 0x21
        })
    )

    let pin = createRelay(1, virtual)

    // alternaitve
    this.repl.inject({
        // pins,
        pin
    })
})

const app = express()

app.use(cors({origin: '*'}))

app.get('/pins', (req, res) => {
    res.send(pins.map(({operation, ...pin}) => ({
        ...pin,
        isOn: operation.isOn,
    })))
})

function getPinByGPIO(gpio) {
    gpio = parseInt(gpio)
    return pins.find(pin => pin.gpio == gpio)
}

app.get('/:id', (req, res) => {
    let isOn = getPinByGPIO(req.params.id).operation.isOn
    res.send(isOn)
})

app.get('/:id/on', (req, res) => {
    getPinByGPIO(req.params.id).operation.on()
    res.send('on')
})

app.get('/:id/off', (req, res) => {
    getPinByGPIO(req.params.id).operation.off()
    res.send('off')
})

app.listen(80)
