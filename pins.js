module.exports = [
    {
        gpio: 4,
        name: 'PIR',
        type: 'motion',
    },
    {
        gpio: 21,
        name: 'Relais 1',
        type: 'relay',
    },
    {
        gpio: 20,
        name: 'Relais 2',
        type: 'relay',
    },
    {
        gpio: 19,
        name: 'Relais 3',
        type: 'relay',
    },
    {
        gpio: 26,
        name: 'Relais 4',
        type: 'relay',
    },
    {
        gpio: 13,
        name: 'Relais 5',
        type: 'relay',
    },
    {
        gpio: 16,
        name: 'Relais 6',
        type: 'relay',
    },
    {
        gpio: 5,
        name: 'Relais 7',
        type: 'relay',
    },
    {
        gpio: 6,
        name: 'Relais 8',
        type: 'relay',
    },
    {
        gpio: 7,
        name: 'Relais 9',
        type: 'relay',
    },
    {
        gpio: 12,
        name: 'Relais 10',
        type: 'relay',
    },
    {
        gpio: 25,
        name: 'Relais 11',
        type: 'relay',
    },
    {
        gpio: 8,
        name: 'Relais 12',
        type: 'relay',
    },
    {
        gpio: 9,
        name: 'Relais 13',
        type: 'relay',
    },
    {
        gpio: 11,
        name: 'Relais 14',
        type: 'relay',
    },
    {
        gpio: 10,
        name: 'Relais 15',
        type: 'relay',
    },
    {
        gpio: 24,
        name: 'Relais 16',
        type: 'relay',
    },
]